package com.fantcavodi.asta.model;

public class Giocatore {
	
	private String ruolo;
	private String nome;
	private String squadra;
	private Integer qt;
	private Integer id;
	private String message;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRuolo() {
		return ruolo;
	}
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSquadra() {
		return squadra;
	}
	public void setSquadra(String squadra) {
		this.squadra = squadra;
	}
	public Integer getQt() {
		return qt;
	}
	public void setQt(Integer qt) {
		this.qt = qt;
	}

}
