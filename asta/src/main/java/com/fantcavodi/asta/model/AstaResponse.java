package com.fantcavodi.asta.model;

import java.math.BigDecimal;

public class AstaResponse {
	
	private BigDecimal timestamp;
	private String message;
	private Giocatore giocatore;
	
	
	public AstaResponse(BigDecimal timestamp, String message, Giocatore giocatore) {
		this.giocatore = giocatore;
		this.message = message;
		this.timestamp = timestamp;
	}
	
	public AstaResponse(BigDecimal timestamp, String message) {
		this.message = message;
		this.timestamp = timestamp;
	}
	
	public BigDecimal getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(BigDecimal timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Giocatore getGiocatore() {
		return giocatore;
	}
	public void setGiocatore(Giocatore giocatore) {
		this.giocatore = giocatore;
	}
	

}
