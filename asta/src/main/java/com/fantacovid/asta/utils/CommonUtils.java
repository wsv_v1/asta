package com.fantacovid.asta.utils;

import org.bson.Document;

import com.google.gson.Gson;

public class CommonUtils {
	
	public static Document getConvertedDocument(Object object) {

		String insertJson = new Gson().toJson(object);
		Document docTeam = Document.parse(insertJson);
	   
	    return docTeam;
    
	}

}
