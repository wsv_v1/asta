package com.fantacovid.asta.utils;

public class Constants {
	
	public static final String COLLECTION_TV_SERIES = "TV_SERIES";
	public static final String COLLECTION_TV_SERIES_UNWATCHED = "TV_SERIES_UNWATCHED";
	public static final int CODE_ERROR_GENERIC = 100;
	public static final int STATUS_CODE_OK = 200;

}
