package com.fantacovid.asta.converter;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import com.fantcavodi.asta.model.Giocatore;

public class ObjectConverter {
	
	public static Giocatore convertFileToObject(XSSFRow row, int randoNum) {
		Giocatore giocatore = new Giocatore();
		
		XSSFCell cellaRuolo = row.getCell(0);
		String ruolo = cellaRuolo.getStringCellValue();
		giocatore.setRuolo(ruolo);
		
		XSSFCell cellaNome = row.getCell(1);
		String nome = cellaNome.getStringCellValue();
		giocatore.setNome(nome);
		
		XSSFCell cellaSquadra = row.getCell(2);
		String squadra = cellaSquadra.getStringCellValue();
		giocatore.setSquadra(squadra);
		
		XSSFCell cellaQt = row.getCell(3);
		Double valore = cellaQt.getNumericCellValue();
		Integer qt = valore.intValue();
		giocatore.setQt(qt);
		
		giocatore.setId(randoNum);
				
		return giocatore;		
	}

}
