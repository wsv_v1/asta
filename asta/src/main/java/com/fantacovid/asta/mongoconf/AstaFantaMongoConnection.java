package com.fantacovid.asta.mongoconf;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;

@Configuration
public class AstaFantaMongoConnection {
	
	@Autowired
	private AstaFantaYamlConfiguration yamlConf;
	
	@Bean
	public MongoConnectionSkeleton getMongoDBConnection() {
		MongoClientOptions.Builder mdBuilder = new MongoClientOptions.Builder();
		mdBuilder.maxConnectionIdleTime(yamlConf.getIdleTimeConn()).sslEnabled(yamlConf.isSslConnEn());
		MongoClientOptions opts = mdBuilder.build();
		
		MongoCredential credential = MongoCredential.createCredential(yamlConf.getUserDbMongo(), yamlConf.getRootUser(), yamlConf.getPasswordDbMongo().toCharArray());
		
		MongoConnectionSkeleton mongoConnectionSkeleton = new MongoConnectionSkeleton(opts, yamlConf, credential);
		
		return mongoConnectionSkeleton;
	}
	
	public MongoClient getDatabaseConnection(MongoCredential credential, String clusterMongoIp, int clusterMongoPort, MongoClientOptions opts) {
		
		MongoClient mc = null;

		try {
			mc = new MongoClient(new ServerAddress(clusterMongoIp, clusterMongoPort), Arrays.asList(credential), opts);
		} catch (MongoException e) {
			e.printStackTrace();
			System.out.println("Connection failed");
		} 
		
		return mc;
	}

}
