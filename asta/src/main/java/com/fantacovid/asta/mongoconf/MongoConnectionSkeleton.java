package com.fantacovid.asta.mongoconf;

import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;

public class MongoConnectionSkeleton {
	
	private MongoClientOptions opts;
	private AstaFantaYamlConfiguration yamlCon;
	private MongoCredential credential;
	
	
	public MongoConnectionSkeleton(MongoClientOptions opts, AstaFantaYamlConfiguration yamlCon, MongoCredential credential) {
		this.credential = credential;
		this.opts = opts;
		this.yamlCon = yamlCon;
	}
	
	public MongoClientOptions getOpts() {
		return opts;
	}
	public void setOpts(MongoClientOptions opts) {
		this.opts = opts;
	}
	public AstaFantaYamlConfiguration getYamlCon() {
		return yamlCon;
	}
	public void setYamlCon(AstaFantaYamlConfiguration yamlCon) {
		this.yamlCon = yamlCon;
	}
	public MongoCredential getCredential() {
		return credential;
	}
	public void setCredential(MongoCredential credential) {
		this.credential = credential;
	}

}
