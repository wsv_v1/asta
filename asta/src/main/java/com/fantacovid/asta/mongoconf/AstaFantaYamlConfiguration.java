package com.fantacovid.asta.mongoconf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class AstaFantaYamlConfiguration {
	
	private String rootUser;
	private String userDbMongo;
	private String passwordDbMongo;
	private String clusterMongoIp;
	private int clusterMongoPort;
	private int idleTimeConn;
	private boolean sslConnEn;
	private String databaseName;
	
	
	public String getRootUser() {
		return rootUser;
	}
	public void setRootUser(String rootUser) {
		this.rootUser = rootUser;
	}
	public String getUserDbMongo() {
		return userDbMongo;
	}
	public void setUserDbMongo(String userDbMongo) {
		this.userDbMongo = userDbMongo;
	}
	public String getPasswordDbMongo() {
		return passwordDbMongo;
	}
	public void setPasswordDbMongo(String passwordDbMongo) {
		this.passwordDbMongo = passwordDbMongo;
	}
	public String getClusterMongoIp() {
		return clusterMongoIp;
	}
	public void setClusterMongoIp(String clusterMongoIp) {
		this.clusterMongoIp = clusterMongoIp;
	}
	public int getClusterMongoPort() {
		return clusterMongoPort;
	}
	public void setClusterMongoPort(int clusterMongoPort) {
		this.clusterMongoPort = clusterMongoPort;
	}
	public int getIdleTimeConn() {
		return idleTimeConn;
	}
	public void setIdleTimeConn(int idleTimeConn) {
		this.idleTimeConn = idleTimeConn;
	}
	public boolean isSslConnEn() {
		return sslConnEn;
	}
	public void setSslConnEn(boolean sslConnEn) {
		this.sslConnEn = sslConnEn;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

}
