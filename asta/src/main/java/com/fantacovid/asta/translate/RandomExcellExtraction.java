package com.fantacovid.asta.translate;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fantacovid.asta.converter.ObjectConverter;
import com.fantcavodi.asta.model.Giocatore;

public class RandomExcellExtraction {

	private static final String FILE_NAME = "./risorse/excell/Quotazioni_Fantacalcio.xlsx";

	public static void main(String[] args) {

		JFrame f = new JFrame();
		
		ArrayList<String> listaAllenatori = new ArrayList<String>();
		listaAllenatori.add("ANDREA");
		listaAllenatori.add("LUCA");
		listaAllenatori.add("DANILO");
		listaAllenatori.add("LELLO");
		listaAllenatori.add("CRISH");
		listaAllenatori.add("ALESSIO");
		listaAllenatori.add("KRISTIAN");
		listaAllenatori.add("TONY");
		listaAllenatori.add("MATTEO");
		listaAllenatori.add("RICCARDO");
		
		JLabel allenatore = new JLabel();
		allenatore.setBounds(150, 50, 250, 30);
		allenatore.setFont(allenatore.getFont().deriveFont(16.0f));
		
		JLabel indice = new JLabel();
		indice.setBounds(120, 50, 30, 30);
		indice.setFont(allenatore.getFont().deriveFont(16.0f));
		
		JLabel totale = new JLabel();
		totale.setBounds(150, 320, 220, 30);
		totale.setFont(allenatore.getFont().deriveFont(16.0f));
		totale.setForeground(Color.GREEN);
		
		JButton estrai = new JButton("ESTRAI");
		estrai.setBounds(50, 100, 100, 40);
		JButton elimina = new JButton("ELIMINA");
		elimina.setBounds(160, 100, 100, 40);
		elimina.setEnabled(false);
		
		JRadioButton rb = new JRadioButton("tieni");
		rb.setBounds(300, 100, 100, 20);
		
		ArrayList<JLabel> tfs = new ArrayList<JLabel>();
		
		int y = 180;
		
		for (int i = 0; i < 6; i++) {
			JLabel tf = new JLabel();
			tf.setBounds(70, y, 250, 30);
			tf.setFont(tf.getFont().deriveFont(16.0f));
			if(i == 5) {
				tf.setForeground(Color.RED);
			}
            tfs.add(tf);
            y = y + 20; 
        }

		f.add(estrai);

		estrai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Giocatore giocatore = excellToList(0, totale);
				if(giocatore.getMessage() == null) {
					tfs.get(0).setText("NOME: " + giocatore.getNome());
					tfs.get(1).setText("SQUADRA: " + giocatore.getSquadra());
					tfs.get(2).setText("RUOLO: " + giocatore.getRuolo());
					tfs.get(3).setText("QUOTAZIONE: " + giocatore.getQt());
					tfs.get(4).setText("ID: " + giocatore.getId());
					tfs.get(5).setText("");
					elimina.setEnabled(true);
				} else {
					tfs.get(5).setText(giocatore.getMessage());
				}
				if(indice.getText() == null || indice.getText().equals("") || indice.getText().equals("10")) {
					indice.setText("1");
				} else {
					int indiceInt = Integer.parseInt(indice.getText()) + 1;
					indice.setText(String.valueOf(indiceInt));
				}
				
				allenatore.setText(listaAllenatori.get(Integer.parseInt(indice.getText())-1));
				
				estrai.setEnabled(false);
			}
		});
		
		elimina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean isTieni = rb.isSelected();
				String label = tfs.get(4).getText().substring(4);
				String message = deleteFromExcell(Integer.parseInt(label), isTieni);
				tfs.get(0).setText("");
				tfs.get(1).setText("");
				tfs.get(2).setText("");
				tfs.get(3).setText("");
				tfs.get(4).setText("");
				tfs.get(5).setText(message);
				elimina.setEnabled(false);	
				estrai.setEnabled(true);
				totale.setText("");
			}
		});

		for (JLabel tf : tfs) {
            f.getContentPane().add(tf);
        }
		f.add(indice);
		f.add(allenatore);
		f.add(totale);
		f.add(elimina);
		f.add(rb);

		f.setSize(400, 500);
		f.setLayout(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
	}

	public static Giocatore excellToList(int numberOfSheet, JLabel totale) {

		XSSFWorkbook workbook = null;
		Giocatore giocatore = null;

		try {
			String separator = File.separator;
			FileInputStream excellInput = new FileInputStream(new File(FILE_NAME.replace("/", separator)));
			workbook = new XSSFWorkbook(excellInput);
			XSSFSheet sheet0 = workbook.getSheetAt(numberOfSheet);

			int lastRow = sheet0.getLastRowNum();
			totale.setText("GIOCATORI RIMASTI: " + String.valueOf(lastRow-1));

			int randomNum = 1;

			if (lastRow > 1) {
				randomNum = ThreadLocalRandom.current().nextInt(1, lastRow);
			} else if (lastRow < 1) {
				Giocatore giocatore2 = new Giocatore();
				giocatore2.setMessage("GIOCATORI TERMINATI!!!!");
				return giocatore2;
			}

			XSSFRow row = sheet0.getRow(randomNum);
			
			if (row != null && row.getCell(0) != null) {
				giocatore = ObjectConverter.convertFileToObject(row, randomNum);
			}

		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return giocatore;

	}

	public static String deleteFromExcell(int id, boolean isTieni) {

		XSSFWorkbook workbook = null;

		try {
			String separator = File.separator;
			FileInputStream excellInput = new FileInputStream(new File(FILE_NAME.replace("/", separator)));
			workbook = new XSSFWorkbook(excellInput);
			XSSFSheet sheet0 = workbook.getSheetAt(0);

			XSSFRow row = sheet0.getRow(id);

			if (isTieni) {
				XSSFSheet sheet2 = workbook.getSheetAt(1);
				int lastRow = sheet2.getLastRowNum();
				XSSFRow row2 = sheet2.createRow(lastRow + 1);
				for (int i = 0; i < 4; i++) {
					XSSFCell cell = row2.createCell(i);
					if (i < 3) {
						cell.setCellValue(row.getCell(i).getStringCellValue());
					} else {
						cell.setCellValue(row.getCell(i).getNumericCellValue());
					}
				}
			}

			int count = sheet0.getLastRowNum();

			if (id > 0 && id < count) {
				sheet0.shiftRows(id + 1, count, -1);
			} else {
				sheet0.removeRow(row);
			}

			try (FileOutputStream outputStream = new FileOutputStream("./risorse/excell/Quotazioni_Fantacalcio.xlsx")) {
				workbook.write(outputStream);
			}

		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "Calciatore eliminato/spostato";
	}
}
